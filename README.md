# Supplementary material: A computational reproducibility study of PLOS ONE articles featuring longitudinal data analyses

This repository contains all information needed to reproduce the results of our
article *A computational reproducibility study of PLOS ONE articles featuring longitudinal data analyses*.
Furthermore it entails detailed reports on the methods used in each of the 
articles we looked at as well as the steps taken for reproducing them.


## Setup

Each article folder in `/papers` contains a `README.md`, a
`/data` folder and an `/analysis` folder.  The `README.md`
contains information on the paper and the students who worked on reproducing the
paper, i.e.  Title; Author(s); Year; DOI; Link; Link to data; Software used;
Code available (yes/no); Link to code; Contact e-mail; Student name(s).  The
`/data` folder contains sub-folders `/data\_raw` and
`/data\_clean` with the raw and clean data respectively as well as the
code to clean the data (`clean\_data.Rmd`).  The `/analysis`
folder contains a file with the description and code of the statistical
analysis (`analysis.Rmd`). We use RMarkdown (`Rmd`) for
*literate programming* to allow for better communication of the source
code and the results obtained. Another nice feature of RMarkdown is the
possibility to render to HTML, PDF or Word.

