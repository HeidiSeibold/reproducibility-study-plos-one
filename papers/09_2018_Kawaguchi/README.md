# Reproducibility study: paper 09

**This reproducibility study is built on the R-Markdown extension `bookdown`**.

**To execute the essay_09.Rmd-file please execute `install.packages("bookdown")` beforehand**!

### Paper details

- Title: A Time-Lagged Effect of Conspecific Density on Habitat
Selection by Snowshoe Hare.
- Author(s): Kawaguchi, Toshinori; Desrochers, André 
- Year: 2018
- Journal: PLOS ONE
- DOI: 10.1371/journal.pone.0190643
- Link: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5761860/
- Data available (yes/no): yes
- Link to data: https://doi.org/10.1371/journal.pone.0190643.s004 ; https://doi.org/10.1371/journal.pone.0190643.s005 
- Software used: R
- Code available (yes/no): yes
- Link to code: no link; provided through email; avaiable in folder "analysis/Author material"
- Contact e-mail: tkawaguchi907@gmail.com


### Project details

- Student name(s): Forkel, Amelie; Kopper, Philipp; Piehler, Alexander
