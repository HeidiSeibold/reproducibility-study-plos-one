# Estimating Body Composition in Adolescent Sprint Athletes: Comparison of Different Methods in a 3 Years Longitudinal Design

### Paper details

- Title: Estimating Body Composition in Adolescent Sprint Athletes: Comparison of Different Methods in a 3 Years Longitudinal Design
- Author(s): Dirk Aerenhouts, Peter Clarys, Jan Taeymans, Jelle Van Couwenberg
- Year: 2015
- Journal: PLos One 10(8)
- DOI:10.1371/journal.pone.0136788
- Link:https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0136788
- Data available (yes/no):yes
- Link to data:https://journals.plos.org/plosone/article/file?type=supplementary&id=info:doi/10.1371/journal.pone.0136788.s001
- Software used:SPSS, SAS
- Code available (yes/no):no
- Link to code:
- Contact e-mail:dirk.aerenhouts@vub.ac.be


### Project details

- Student name(s): Ava Arshadipour, Patricia Haro Lara, Antje Völker
