# Optimizing community screening for tubercolosis: Spatial analysis of localized case finding from door-to-door screening for TB in an urban district of Ho Chi Minh City, Viet Nam

### Paper details

- Title: Optimizing community screening for tubercolosis: Spatial analysis of localized case finding from door-to-door screening for TB in an urban district of Ho Chi Minh City, Viet Nam
- Author(s): Luan Nguyen Quang Vo, Thanh Nguyen Vu, Hoa Trung Nguyen, Tung
Thanh Truong, Canh Minh Khuu, Phuong Quoc Pham, Lan Huu Nguyen, Giang
Truong Le, Jacob Creswell
- Year: 2018
- Journal: PLOSONE
- DOI:
- Link: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0209290
- Data available (yes/no): yes
- Link to data: https://doi.org/10.1371/journal.pone.0209290.s001
- Software used: not mentioned in the paper
- Code available (yes/no): no
- Link to code: /
- Contact e-mail: luan.vo@tbhelp.org


### Project details

- Student name(s): Joshua Wagner, Nico Hahn, Simone Zellner
