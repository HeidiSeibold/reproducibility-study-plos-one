# Morphometry Predicts Early GFR Change in Primary Proteinuric Glomerulopathies: A Longitudinal Cohort Study Using GEE

### Paper details

- Title: Morphometry Predicts Early GFR Change in Primary Proteinuric Glomerulopathies: A Longitudinal Cohort Study Using Generalized Estimating Equations
- Author(s): 
    Kevin V. Lemley ,
    Serena M. Bagnasco,
    Cynthia C. Nast,
    Laura Barisoni,
    Catherine M. Conway,
    Stephen M. Hewitt,
    Peter X. K. Song
- Year: 2016
- Journal: Plos One
- DOI: 10.1371/journal.pone.0157148
- Link: https://doi.org/10.1371/journal.pone.0157148 
- Data available (yes/no): yes
- Link to data: https://doi.org/10.1371/journal.pone.0157148.s001
- Software used: R, SAS
- Code available (yes/no): no
- Link to code: -
- Contact e-mail: klemley@chla.usc.edu


### Project details

- Student name(s): Thomas Eder, Moritz Wagner, Stefanie Schmid
