## Models
This section introduces the GEE models calculated by @rawson15 more detailed and presents the results of our reproducibility work.

## Hypotheses
Recall that the authors conducted the study with three main hypotheses:

1. Carriers of **BDNF** Met/Met genotype show increased risk of depressive symptoms after hip fracture
2. Carriers of **5HTTLPR** S allele show increased risk of depressive symptoms after hip fracture
3. Carriers of **5HT1a** G allele show increased risk of depressive symptoms after hip fracture

Note that the S and G alleles are present in two genotype, while Met/Met is a single genotype.

## Original statistical analysis

To examine neurotrophic and serotonin-related depressive symptoms, @rawson15 calculated a different number of GEE models. To reduce variance introduced by genetic variability, only white patients with hip fracture were used as a sample base ($n$ = 466).

At first, three GEE models were fit predicting **mastot** by **timepoint** and **BDNF** genotype, by **timepoint** and **5HTTLPR** genotype and by **timepoint** and **5HT1a** genotype. Then, a fourth model was fit using **timepoint** and the interaction between **BDNF** and **5HTTLPR**.

Significance of the results was assessed via corrected $p$-values. Due to multiple hypothesis testing ($m$ = 3), parameter estimates were considered significant for $p^* < \frac{0.05}{m} \approx 0.016$ (Bonferroni correction).

In a next step the authors fitted several GEE models to evaluate the influence of **gender**, **agegroup**, **education**, **antidepressant** use and the **Short-Blessed Test** scores on MADRS scores. Since only **antidepressant** use proved to be statistically significant, the genetic polymorphisms of the first four models were re-examined including **timepoint** and **antidepressant** use as covariates.

Finally the authors used a more narrow subsample than the original one, including only patients who perceived the hip fracture event as "somewhat stressful" or "very stressful" (**PSS1_** = 2, 3). This excluded five percent of the patients. Again including **timepoint** and **antidepressant** use as a covariate, the first four models were re-fit using the new subsample.

## Technical aspects
@rawson15 used SAS 9.3 procedure GENMOD (SAS Institute, Cary, NC) to calculate the generalized estimating equations (GEE) models with Poisson distribution. The procedure accounts for missing data (study dropout) using the all available pairs method.

Since the longitudinal data is correlated (MADRS scores of the same subject are similar), the following AR(1) correlation structure is assumed for the four time points:

$$\boldsymbol \sigma^2 \begin{pmatrix}
1.0 & \rho & \rho^2 & \rho^3\\ \\
 & 1.0 & \rho & \rho^2\\ \\
 & & 1.0 & \rho \\ \\
 & & & 1.0
\end{pmatrix}$$

Here, $\rho$ denotes the correlation coefficient between to adjacent time points: $\rho = Corr(y_{i,j},y_{i,j+1})$. As can be seen, this assumes the correlation of within-subject MADRS scores decreases with increasing time difference.

Due to the fact that GEE models are not Likelihood-based, information-based model selection criteria like AIC or BIC are not available. Thus, variable selection based on AIC or BIC cannot be performed. An alternative to GEEs are *generalized linear mixed models* (GLMMs) which include random effects to estimate within-subject variability. For GLMMs, variable selection via usual procedures can be performed.

## Reproduced models
We reproduced the first three models with the **timepoint** and genetic polymorphisms and the fourth model with **timepoint** and the interaction between **BDNF** and **5HTTLPR** as covariates using R 3.6.1 software and the geepack package version 1.2-1 [@geepack]. Genotyping among hip fracture patients produces some NA values for the polymorphisms which are excluded in the following analysis.

### (M1) BDNF neurotrophic factor
The first step is to transform the data to fit the data structure required by the geepack package:

```{r}
## subset to white only and time points BL - W4
datw <- dat[dat$racebl == "white" & dat$timepoint %in% c("BL", "W1", "W2","W4"), ]
datw <- droplevels(datw)

## set default levels
datw$BDNF <- factor(datw$BDNF, levels = c("V/V", "V/M", "M/M"))
datw$HTTLPR <- factor(datw$HTTLPR, levels = c("LA/LA", "S/LA", "S/S"))
datw$HT1a <- factor(datw$HT1a, levels = c("C/C", "C/G", "G/G"))
datw$timepoint <- factor(datw$timepoint, levels = c("W4", "BL", "W1", "W2"))

## geepack requires quite a specific data structure, i.e.,
## no NA with respect to predictors and no unused levels
## BDNF no NA only
datwBDNF <- datw[datw$group == "hip_fracture", ]
datwBDNF <- datwBDNF[- which(is.na(datwBDNF$BDNF)), ]
datwBDNF$id <- droplevels(datwBDNF$id)
datwBDNF <- datwBDNF[order(datwBDNF$id), ]
```

As we can see, $n = 429$ patients with BDNF genotype remain:

```{r}
length(unique(datwBDNF$id)) # 429 participants remaining
```

After that, we fit the model. The function call is similar to the $glm()$ function, the only difference being the $id$ argument for specifying subject identification variable and the $corstr$ argument for specifying the correlation structure of the model.

```{r}
m1 <- geeglm(mastot ~ timepoint + BDNF, family = poisson, data = datwBDNF,
  id = id, corstr = "ar1")
summary(m1)
```

The summary shows the log parameter estimates, robust standard errors and, in contrast to the SAS genmod procedure, a Wald test for significance.

Comparing the original results and ours shows that we can reproduce the estimates up to two decimal points. The largest difference is $0.02$ ($\hat{\beta}_{\mathrm{BDNF_{Met/Met\_Val/Val}}} - \hat{\beta^*}_{\mathrm{BDNF_{Met/Met\_Val/Val}}}$).

Significance is originally assessed via Z-tests [@rawson15]. Thus, the Z-statistic in @rawson15 can be obtained by $\sqrt{Wald}$. While $Z \stackrel{a}{\sim} N(0,1)$, it holds that $W \stackrel{a}{\sim} \chi^2(1)$.
We see indeed that e.g. $Z_{\hat{\beta_0}} = 31.77 \approx \sqrt{W_{\hat{\beta_0}}} = \sqrt{1012.673} = 31.82$. As expected, reproduced significant findings are the same as in @rawson15: $\hat{\beta_0}$ is highly significant, with an estimated mean MADRS score of $\exp(1.72) = 5.58$ for Val/Val carriers at week 4. **BDNF** Met/Met carriers show significantly higher MADRS scores than Val/Val carriers: the estimated mean MADRS score is $\exp(0.37) = 1.45$ times higher at week 4.

### (M2) 5HTTLPR genotype

The second model investigates the relationship between **MADRS score** and **time** and the **5HTTLPR** genotype ($n$ = 434). 

```{r}
## HTTLPR no NA only
datwHTTLPR <- datw[datw$group == "hip_fracture", ]
datwHTTLPR <- datwHTTLPR[- which(is.na(datwHTTLPR$HTTLPR)), ]
datwHTTLPR$id <- droplevels(datwHTTLPR$id)
datwHTTLPR <- datwHTTLPR[order(datwHTTLPR$id), ]
length(unique(datwHTTLPR$id)) # 434 participants remaining
```

This is the function call:

```{r}
m2 <- geeglm(mastot ~ timepoint + HTTLPR, family = poisson, data = datwHTTLPR,
  id = id, corstr = "ar1")
summary(m2)
```

Comparing the original results and ours, the difference in point estimates is not larger than $0.01$. Both carriers of the S/S genotype and the LA/S genotype show significantly lower MADRS scores than carriers of the LA/LA genotype at week 4 (by a factor of $\exp(-0.20) = 0.81$).

### (M3) 5HT1a genotype

The third model investigates the relationship between **MADRS score** and **time** and the **5HT1a** genotype. ($n$ = 424). 

```{r}
## HT1a no NA only
datwHT1a <- datw[datw$group == "hip_fracture", ]
datwHT1a <- datwHT1a[- which(is.na(datwHT1a$HT1a)), ]
datwHT1a$id <- droplevels(datwHT1a$id)
datwHT1a <- datwHT1a[order(datwHT1a$id), ]
length(unique(datwHT1a$id)) # 424 participants remaining
```

The function call is as follows:

```{r}
m3 <- geeglm(mastot ~ timepoint + HT1a, family = poisson, data = datwHT1a,
  id = id, corstr = "ar1")
summary(m3)
```

We can reproduce the point estimates of @rawson15 with a difference of $0.01$. Note that in this model, there is no significant relationship between the **5HT1a** genotype and MADRS score.

### (M4) Interaction between BDNF factor and 5HTTLPR genotype

The fourth model displays the interaction effect between between **BDNF factor** and **5HTTLPR** genotype on depressive symptoms. ($n$ = 428). 

```{r}
## BDNF and HTTLPR no NA only
datwBH <- datw[datw$group == "hip_fracture", ]
datwBH <- datwBH[- which(is.na(datwBH$BDNF) | is.na(datwBH$HTTLPR)), ]
datwBH$id <- droplevels(datwBH$id)
datwBH <- datwBH[order(datwBH$id), ]
length(unique(datwBH$id)) # 428 participants remaining
```

Fitting the model is as follows:

```{r}
m4 <- geeglm(mastot ~ timepoint + BDNF * HTTLPR, family = poisson, data = datwBH,
  id = id, corstr = "ar1")
summary(m4)
```

As we can see, we can reproduce the point estimates of @rawson15 by a difference of at most $0.04$. Within the LA/LA subgroup, carriers of the Val/Val genotype show significantly lower (by a factor of $\exp(-0.89) = 0.45$) depressive symptoms than carriers of the Met/Met genotype, compared to the S/S subgroup, where there is no observed difference.

