# < Association of Functional Polymorphisms from Brain-Derived Neurotrophic Factor and Serotonin-Related Genes with Depressive Symptoms after a Medical Stressor in Older Adults >

### Paper details

- Title: Association of Functional Polymorphisms from Brain-Derived Neurotrophic Factor and Serotonin-Related Genes with Depressive Symptoms after a Medical Stressor in Older Adults
- Author(s): Rawson, Dixon, Nowotny, Ricci, Binder, Rodebaugh, Wendleton, Doré and Lenze
- Year: 2015
- Journal: PLoS One
- DOI: 10.1371/journal.pone.0120685
- Link: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0120685
- Data available (yes/no): yes
- Link to data: https://digitalcommons.wustl.edu/open_access_pubs/3555/
- Software used: SAS 9.3 and SPSS 21.0, Structure Software Version 2.3
- Code available (yes/no): no
- Link to code:
- Contact e-mail: rawson@wustl.edu

### Project details

- Student name(s): Roman Dieterle, Lennart Schneider and Rui Yang
