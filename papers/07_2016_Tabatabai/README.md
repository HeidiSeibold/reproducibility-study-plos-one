# < Racial and Gender Disparities in Incidence of Lung and Bronchus Cancer in the United States: A Longitudinal Analysis >

### Paper details

- Title: Racial and Gender Disparities in Incidence of Lung and Bronchus Cancer in the United States: A Longitudinal Analysis Mohammad A. Tabatabai1, Jean-Jacques Kengwoung-Keumo2, Gab
- Author(s): Mohammad A. Tabatabai, Jean-Jacques Kengwoung-Keumo, Gabriela R. Oates,
Juliette T. Guemmegne, Akinola Akinlawon, Green Ekadi, Mona N. Fouad, Karan P. Singh
- Year: 2016
- Journal: PLOS ONE
- DOI:10 1371/journal.pone.0162949
- Link: https://doi.org/10.1371/journal.pone.0162949 
- Data available (yes/no): yes
- Link to data: http://wonder.cdc.gov/cancer-v2012.html
- Software used: SAS version 9.3, SPSS version 23, and Mathematica
version 10
- Code available (yes/no): no
- Link to code: -
- Contact e-mail: kpsingh@uab.edu


### Project details

- Student name(s): Schmidt, To, Waize 
