---
title: "Clean datasets"
author: 
output:
  pdf_document: default
  html_document: default
--- 


```{r}
# a bunch of required packages
packages_required <- c(
  "readxl",
  "zoo"
)

# check which ones are not installed
not_installed <- packages_required[!packages_required %in%
                                     installed.packages()[, "Package"]]
# install them
if (length(not_installed) > 0) {
  lapply(not_installed, install.packages)
}
# load them
lapply(packages_required, library, character.only = TRUE)

#########################################################################################
##     Preprocessing dataset of lung cancer incidence                                  ##
#########################################################################################

# import incidence dataset
Cancer <-
  read.delim(
    "./data/data_raw/Cancer_complete.txt")

# only certain states used
data <-
  Cancer[Cancer$State == "Alabama" |
           Cancer$State == "Arizona" |
           Cancer$State == "Arkansas" |
           Cancer$State == "California" |
           Cancer$State == "Colorado" |
           Cancer$State == "Connecticut" |
           Cancer$State == "Delaware" |
           Cancer$State == "Florida" |
           Cancer$State == "Georgia" |
           Cancer$State == "Illinois" |
           Cancer$State == "Indiana" |
           Cancer$State == "Iowa" |
           Cancer$State == "Kansas" |
           Cancer$State == "Kentucky" |
           Cancer$State == "Louisiana" |
           Cancer$State == "Maryland" |
           Cancer$State == "Massachusetts" |
           Cancer$State == "Michigan" |
           Cancer$State == "Minnesota" |
           Cancer$State == "Mississippi" |
           Cancer$State == "Missouri" |
           Cancer$State == "Nebraska" |
           Cancer$State == "Nevada" |
           Cancer$State == "New Jersey" |
           Cancer$State == "New York" |
           Cancer$State == "North Carolina" |
           Cancer$State == "Ohio" |
           Cancer$State == "Oklahoma" |
           Cancer$State == "Oregon" |
           Cancer$State == "Pennsylvania" |
           Cancer$State == "Rhode Island" |
           Cancer$State == "South Carolina" |
           Cancer$State == "Tennessee" |
           Cancer$State == "Texas" |
           Cancer$State == "Virginia" |
           Cancer$State == "Washington" |
           Cancer$State == "West Virginia" |
           Cancer$State == "Wisconsin" | Cancer$State == "District of Columbia", ]

# only races black and white 
data <- data[data$Race == "Black or African American" | data$Race == "White", ]

# delete redundandent variables
data <- data[,c("Year", "State", "Race", "Sex", "Age.Adjusted.Rate")]

summary(data)

# create new "region"-variable as used in the paper
data$Region <- NA
for (i in 1:nrow(data)) {
  if (data$State[i] == "Connecticut" ||
      data$State[i] == "Rhode Island" ||
      data$State[i] == "Massachusetts") {
    data$Region[i] = "New England"
  }
  if (data$State[i] == "District of Columbia" ||
      data$State[i] == "New Jersey" ||
      data$State[i] == "New York" || data$State[i] == "Pennsylvania") {
    data$Region[i] = "Mid-Atlantic"
  }
  if (data$State[i] == "Illinois" ||
      data$State[i] == "Indiana" ||
      data$State[i] == "Iowa" ||
      data$State[i] == "Kansas" ||
      data$State[i] == "Michigan" ||
      data$State[i] == "Minnesota" ||
      data$State[i] == "Missouri" ||
      data$State[i] == "Nebraska" ||
      data$State[i] == "Ohio" || data$State[i] == "Wisconsin") {
    data$Region[i] = "Midwest"
  }
  if (data$State[i] == "California" ||
      data$State[i] == "Oregon" || data$State[i] == "Washington") {
    data$Region[i] = "Pacific Coast"
  }
  if (data$State[i] == "Colorado" || data$State[i] == "Nevada") {
    data$Region[i] = "Rocky Mountain"
  }
  if (data$State[i] == "Alabama" ||
      data$State[i] == "Arkansas" ||
      data$State[i] == "Kentucky" ||
      data$State[i] == "Louisiana" ||
      data$State[i] == "Mississippi" || data$State[i] == "Tennessee") {
    data$Region[i] = "Mid-South"
  }
  if (data$State[i] == "Delaware" ||
      data$State[i] == "Florida" ||
      data$State[i] == "Georgia" ||
      data$State[i] == "Maryland" ||
      data$State[i] == "North Carolina" ||
      data$State[i] == "South Carolina" ||
      data$State[i] == "Virginia" || data$State[i] == "West Virginia") {
    data$Region[i] = "South"
  }
  if (data$State[i] == "Arizona" ||
      data$State[i] == "Oklahoma" || data$State[i] == "Texas") {
    data$Region[i] = "Southwest"
  }
}

#########################################################################################
##     preprocessing dataset of smoking prevalence                                    ##
#########################################################################################

# import smoking prevalence dataset
prev <-
  read_excel("./data/data_raw/12963_2013_235_MOESM4_ESM.xlsx",
             range = "A4:T9385")

# Replacing NA with the last value for complete data, as this dataset is merged 
prev$County <- na.locf(prev$County) 

# function for extracting mean, and low and high confidence value
extract <- function(dat, year) { 
  h <- data.frame()
  a <- as.double(sub("\\(.*", "", dat))
  b <- sub(".*\\(", "", dat)
  b <- as.double(sub(",.*", "", b))
  c <- sub(".*\\(.*,", "", dat)
  c <- as.double(sub("\\)", "", c))
  d <- data.frame(a, b, c)
  names(d) <-
    c(
      paste("p", year, "mean", sep = ""),
      paste("p", year, "KIL", sep = ""),
      paste("p", year, "KIH", sep = "")
    )
  return(d)
}

# function for looping the extraction
combine <- function(data) { 
  for (i in 1999:2012) {
    i <- as.name(i)
    prev <- cbind(prev, extract(eval(substitute(i), prev), i))
  }
  return(prev)
}

prev2 <- combine(prev)

# delete old variables
prev2 <- prev2[,-(3:20)] 

# keep seperate values only
prev3 <- prev2[!prev2$Sex == "Both",]
prev3$County <-
  substr(prev3$County, nchar(prev3$County) - 2 + 1, nchar(prev3$County))

# function for calculating the mean
calcmeans <- function() { 
  prev4 <-
    cbind(aggregate(cbind(p1999mean) ~ County + Sex, mean, data = prev3), "1999")
  names(prev4) <- c("County", "Sex", "MeanPrevalence", "Year")
  levels(prev4$Year) <- c(1999:2012)
  for (i in 2000:2012) {
    var <- as.name(paste("p", i, "mean", sep = ""))
    var2 <- as.formula(paste("p", i, "mean ~ County + Sex", sep = ""))
    a <- cbind(aggregate(var2, mean, data = prev3), i)
    names(a) <- c("County", "Sex", "MeanPrevalence", "Year")
    prev4 <- rbind(prev4, a)
  }
  return(prev4)
}


prev4 <- calcmeans()

# keep only states used in the paper
prev4 <-  
  prev4[prev4$County == "AL" |
          prev4$County == "AZ" |
          prev4$County == "AR" |
          prev4$County == "CA" |
          prev4$County == "CO" |
          prev4$County == "CT" |
          prev4$County == "DE" |
          prev4$County == "DC" |
          prev4$County == "FL" |
          prev4$County == "GA" |
          prev4$County == "IL" |
          prev4$County == "IN" |
          prev4$County == "IA" |
          prev4$County == "KS" |
          prev4$County == "KY" |
          prev4$County == "LA" |
          prev4$County == "MD" |
          prev4$County == "MA" |
          prev4$County == "MI" |
          prev4$County == "MN" |
          prev4$County == "MS" |
          prev4$County == "MO" |
          prev4$County == "NE" |
          prev4$County == "NV" |
          prev4$County == "NJ" |
          prev4$County == "NY" |
          prev4$County == "NC" |
          prev4$County == "OH" |
          prev4$County == "OK" |
          prev4$County == "OR" |
          prev4$County == "PA" |
          prev4$County == "RI" |
          prev4$County == "SC" |
          prev4$County == "TN" |
          prev4$County == "TX" |
          prev4$County == "VA" |
          prev4$County == "WA" |
          prev4$County == "WV" | prev4$County == "WI", ]

# recode variable, use real names instead of abbreviations
prev4$County[prev4$County == "AL"] <- "Alabama"
prev4$County[prev4$County == "AZ"] <- "Arizona"
prev4$County[prev4$County == "AR"] <- "Arkansas"
prev4$County[prev4$County == "CA"] <- "California"
prev4$County[prev4$County == "CO"] <- "Colorado"
prev4$County[prev4$County == "CT"] <- "Connecticut"
prev4$County[prev4$County == "DE"] <- "Delaware"
prev4$County[prev4$County == "DC"] <- "District of Columbia"
prev4$County[prev4$County == "FL"] <- "Florida"
prev4$County[prev4$County == "GA"] <- "Georgia"
prev4$County[prev4$County == "IL"] <- "Illinois"
prev4$County[prev4$County == "IN"] <- "Indiana"
prev4$County[prev4$County == "IA"] <- "Iowa"
prev4$County[prev4$County == "KS"] <- "Kansas"
prev4$County[prev4$County == "KY"] <- "Kentucky"
prev4$County[prev4$County == "LA"] <- "Louisiana"
prev4$County[prev4$County == "MD"] <- "Maryland"
prev4$County[prev4$County == "MA"] <- "Massachusetts"
prev4$County[prev4$County == "MI"] <- "Michigan"
prev4$County[prev4$County == "MN"] <- "Minnesota"
prev4$County[prev4$County == "MS"] <- "Mississippi"
prev4$County[prev4$County == "MO"] <- "Missouri"
prev4$County[prev4$County == "NE"] <- "Nebraska"
prev4$County[prev4$County == "NV"] <- "Nevada"
prev4$County[prev4$County == "NJ"] <- "New Jersey"
prev4$County[prev4$County == "NY"] <- "New York"
prev4$County[prev4$County == "NC"] <- "North Carolina"
prev4$County[prev4$County == "OH"] <- "Ohio"
prev4$County[prev4$County == "OK"] <- "Oklahoma"
prev4$County[prev4$County == "OR"] <- "Oregon"
prev4$County[prev4$County == "PA"] <- "Pennsylvania"
prev4$County[prev4$County == "RI"] <- "Rhode Island"
prev4$County[prev4$County == "SC"] <- "South Carolina"
prev4$County[prev4$County == "TN"] <- "Tennessee"
prev4$County[prev4$County == "TX"] <- "Texas"
prev4$County[prev4$County == "VA"] <- "Virginia"
prev4$County[prev4$County == "WA"] <- "Washington"

# reformating sex variable
prev4$Sex <- as.factor(prev4$Sex) 
levels(prev4$Sex) <- c("Female", "Male")
prev4 <- rbind(prev4, prev4)
prev4 <- prev4[order(prev4$Year, prev4$County), ]

#########################################################################################
##     combine datasets                                                                ##
#########################################################################################

# sort dataset to match prev4
data <-
  data[order(data$Year , data$State), ]

#combine incidence data with smoking prevalence
data <- cbind(data, prev4)

#delete redundant variables
data <- data[, -c(7,8,10)]

#delete missing values
data <- data[data$Age.Adjusted.Rate != "Suppressed" ,]
data <- data[data$Age.Adjusted.Rate != "Missing" ,]

summary(data)

write.csv(data, "data_clean/finaldata.csv")

```