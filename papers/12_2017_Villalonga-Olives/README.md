# Longitudinal changes in health related quality of life in children with migrant backgrounds

### Paper details

- Title: Longitudinal changes in health related quality of life in children with migrant backgrounds
- Author(s): Ester Villalonga-Olives, Ichiro Kawachi, Josue Almansa, Nicole von Steinbüchel
- Year: 2017
- Journal: PLoS ONE
- DOI: 10.1371/journal.pone.0170891
- Link: https://doi.org/10.1371/journal.pone.0170891
- Data available (yes/no): Yes  
- Link to data: https://doi.org/10.1371/journal.pone.0170891.s001; https://doi.org/10.1371/journal.pone.0170891.s002
- Software used: M-Plus 7.1
- Code available (yes/no): No
- Link to code: No
- Contact e-mail: ester.villalonga@gmail.com


### Project details

- Student name(s): Siona Decke, Dario Lepke, Verena Loidl
