---
  title: 'Descriptive Analysis'
author: "Dario Lepke, Siona Decke, Verena Loidl"
date: "`r format(Sys.time(), '%B %d, %Y')`"
output:
bookdown::html_document2:
  df_print: paged
number_sections: no
toc: yes
toc_depth: 4
header_includes: \usepackage{mathtools}
bibliography: bibliography.bib
---
  
```{r} 

# dataset longitudinal data
# started on 02.05.2019

# install packages
install.packages("readxl", dependencies = TRUE)
install.packages("ggplot2", dependencies = TRUE)
install.packages("reshape2", dependencies = TRUE)
install.packages("dplyr", dependencies = TRUE)
install.packages("magrittr", dependencies = TRUE)
install.packages("skimr", dependencies = TRUE)
install.packages("ableExtra", dependencies = TRUE)

# load packages
library(readxl)
library(ggplot2)
library(reshape2)
library(dplyr)
library(magrittr)
library(skimr)
library(kableExtra)

# read in data
data <- read_excel("papers/12_2017_Villalonga-Olives/data/data_raw/journal.pone.0170891.s002.xlsx")
# overview of data
dim(data) # 350 obs 16 variables
summary(data)
str(data)

# inspect the QoL measures
# if one of the two datapoints is zero so is the ChangeQL variable
sum(is.na(data$ChangeQL)) #86
sum(is.na(data$T1_QoL) | is.na(data$T2_QoL)) #86
# View(data[,c("T1_QoL", "T2_QoL", "ChangeQL")]) #visualised

# prepare the data for the Qol plot
qol_plot_data <- melt(data[, c("ChangeQL", "Intervention")],
     id.vars = "Intervention")
head(qol_plot_data)
ggplot(qol_plot_data, aes(x = Intervention, y = value, fill = factor(Intervention),
                         group = factor(Intervention))) +
  geom_boxplot() +
  labs(title = "Change in QoL for different Interventions", y = "ChangeQoL") +  
  scale_x_discrete(limits = c("No Intervention", "Musical Activities", "Painting")) +
  scale_fill_brewer(palette = "Accent", guide = FALSE)

# analogue
# change the name to ChangeBEH later ?
# the a is missing
sum(is.na(data$ChngeBEH)) #123
sum(is.na(data$T1_BEH) | is.na(data$T2_BEH)) #123

beh_plot_data <- melt(data[, c("ChngeBEH", "Intervention")],
                      id.vars = "Intervention")
head(beh_plot_data)
ggplot(beh_plot_data, aes(x = Intervention, y = value, fill = factor(Intervention),
                          group = factor(Intervention))) +
  geom_boxplot() +
  labs(title = "Change in BEH for different Interventions", y = "ChangeQoL") +  
  scale_x_discrete(limits = c("No Intervention", "Musical Activities", "Painting")) +
  scale_fill_brewer(palette = "Dark2", guide = FALSE)

# analogue 
sum(is.na(data$ChngeDEV)) #148
sum(is.na(data$T1_DEV) | is.na(data$T2_DEV)) #148

dev_plot_data <- melt(data[, c("ChngeDEV", "Intervention")],
                      id.vars = "Intervention")
ggplot(dev_plot_data, aes(x = Intervention, y = value, fill = factor(Intervention),
                          group = factor(Intervention))) +
  geom_boxplot() +
  labs(title = "Change in DEV for different Interventions", y = "ChangeQoL") +  
  scale_x_discrete(limits = c("No Intervention", "Musical Activities", "Painting")) +
  scale_fill_brewer(palette = "Set2", guide = FALSE)


# density of the difference in QoL
p <- ggplot(data, aes(x = ChangeQL)) + geom_density(color = "darkblue",
                                                    fill = "lightblue") +
  scale_x_continuous(limits = c(-100, 100)) +
  geom_vline(aes(xintercept = mean(ChangeQL, na.rm = TRUE)), color = "blue",
             linetype = "dashed", size = 1)
p


# visualisation no factors defined yet TO DO !!!
bh <- tibble::as_tibble(data)

skimr::skim_to_list(bh) %>%
  .$factor %>%
  knitr::kable(format = 'latex', booktabs = TRUE) %>%
  kableExtra::kable_styling(latex_options = 'HOLD_position', font_size = 7)



DataExplorer::plot_bar(bh, ggtheme = ggpubr::theme_pubr())

skimr::skim_to_list(bh) %>%
  .$numeric %>%
  knitr::kable(format = 'latex', booktabs = TRUE) %>%
  kableExtra::kable_styling(latex_options = 'HOLD_position', font_size = 6)

```