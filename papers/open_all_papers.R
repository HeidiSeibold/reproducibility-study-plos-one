### READ GOOGLE SHEETS WITH PAPER SELECTION INFO

# library("googlesheets")
# library("plyr")
# 
# ## read google sheet
# gs <- gs_url("https://docs.google.com/spreadsheets/d/1FrUuLijtnWsWaIBO8Id36iT6PPsJmV8TKNrCPLzhguw/edit?usp=sharing")
# 
# s1 <- gs_read(gs, ws = "Selection 1")
# s2 <- gs_read(gs, ws = "Selection 2")
# s3 <- gs_read(gs, ws = "Selection 3")
# sel_list <- list(s1, s2, s3)
# 
# ## get number of papers proposed
# nps <- ldply(sel_list, function(x) data.frame(n_proposed = nrow(x), 
#                                               n_selected = sum(x$Use),
#                                               n_responsive = sum(x$Responsive, na.rm = TRUE)))
# np <- colSums(nps)
# 
# ## combine to one data frame of selected papers
# sel <- ldply(sel_list, subset, subset = Responsive == 1, select = c("URL", "Title", "Contact"))
# 
# pt <- ".*?id=|https://doi.org/"
# sel$DOI <- gsub(pattern = pt, replacement = "", sel$URL)
# sel$URL <- paste0("https://doi.org/", sel$DOI)
# write.csv(sel, file = "papers/papers_description.csv", row.names = FALSE)

sel <- read.csv(file = "papers/papers_description.csv", stringsAsFactors = FALSE)

## create list of papers
cat(paste(sel$URL, collapse = "\n "))

## open all papers in browser
# sapply(sel$URL, browseURL)

## open all papers as PDF
# sapply(paste0("https://journals.plos.org/plosone/article/file?id=", sel$DOI, "&type=printable"), browseURL)
       

### create folders and README.md
library("rcrossref")
library("tools")
paper_infos <- cr_cn(dois = sel$DOI, "citeproc-json-ish")

get_relevant_paper_info <- function(x) {

  year <- x$issued$`date-parts`[,1]

  data.frame(DOI = x$DOI,
             author1 = x$author$family[1],
             year = year,
             title = toTitleCase(x$title),
             stringsAsFactors = FALSE)
}

pi <- ldply(paper_infos, get_relevant_paper_info)
pi$id <- formatC(1:12, width = 2, format = "d", flag = "0")
# 
# paper_folder <- paste(pi$id, pi$year, pi$author1, sep = "_")
# 
# lapply(paper_folder, dir.create)
# lapply(paper_folder, function(x) file.copy("0000_example/README.md", to = x))
