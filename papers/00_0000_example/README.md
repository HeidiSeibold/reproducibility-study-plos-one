# < Title >

### Paper details

- Title:
- Author(s):
- Year:
- Journal:
- DOI:
- Link:
- Data available (yes/no):
- Link to data:
- Software used:
- Code available (yes/no):
- Link to code:
- Contact e-mail:


### Project details

- Student name(s):
