# Fluctuations in airway bacterial communities associated with clinical states and disease stages in cystic fibrosis 

### Paper details

- Title: Fluctuations in airway bacterial communities associated with clinical states and disease stages in cystic fibrosis 
- Author(s): Lisa A. Carmody, Lindsay J. Caverly, Bridget K. Foster, Mary A. M. Rogers, Linda M. Kalikin, Richard H. Simon, Donald R. VanDevanter, John J. LiPuma
- Year: 2018
- Journal: Plos One
- DOI: doi.org/10.1371/journal.pone.0194060
- Link: https://doi.org/10.1371/journal.pone.0194060
- Data available (yes/no): yes and no!
- Link to data: https://www.ncbi.nlm.nih.gov/Traces/study/?acc=PRJNA423040&go=go
- Software used: SPSS(v24), mothur (v.1.29)(for DNA)
- Code available (yes/no): no
- Link to code: no code
- Contact e-mail: jlipuma@med.umich.edu


### Project details

- Student name(s): Chris Heindl, Severin Czerny, Minh Viet Tran
