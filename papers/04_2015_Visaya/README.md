# Reproducibility study: Analysis of Binary Multivariate Longitudinal Data via 2-Dimensional Orbits: An Application to the Agincourt Health and Socio-Demographic Surveillance System in South Africa

### Paper details

- Title: Analysis of Binary Multivariate Longitudinal Data via 2-Dimensional Orbits: An Application to the Agincourt Health and Socio-Demographic
- Author(s): Maria Vivien Visaya, David Sherwell, Benn Sartorius, Fabien Cromieres
- Year: 2015
- Journal: PLoS One
- DOI: 10.1371/journal.pone.0123812
- Link: [paper](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0123812)
- Data available (yes/no): no
- Link to data:
- Software used: Stata (not mentioned in the paper)
- Code available (yes/no): no
- Contact e-mail: [Maria Vivien Visaya](mvvisaya@uj.ac.za) [Benn Sartorius](Benn.Sartorius1@lshtm.ac.uk()


### Project details

- Student name(s): Ruben Hartmann, Steffen Fohr
