##########################################
#                                        #
#         BBmm_approx function           #
#                                        #
##########################################

# Description: BBmm function with transfer of start parameters for the beta coefficients and the variance of the random effects. 
# The aim of this approach is to save computational time
# Note: Same as BBmm function with little change (it marks the part that has been added)

## Input: as BBmm, additionally 
#         start: list of start parameters for beta coefficients and the random effects variance
#                  - first list element:  beta coefficients
#                  - second list element:  random effects variance
## Output: as BBmm

BBmm_start_param <- function (fixed.formula, random.formula = NULL, Z = NULL, nRandComp = NULL, 
          m, data = list(), start, method = "BB-NR", maxiter = 100) 
{
  if ((is.null(random.formula)) & (is.null(Z))) {
    stop("Random part of the model must be especified")
  }
  if ((is.null(random.formula) == FALSE) & (is.null(Z)) == 
      FALSE) {
    stop("Random part of the model has been specified twice")
  }
  if ((is.null(Z) == FALSE) & (is.null(nRandComp))) {
    stop("Number of random components must be specified")
  }
  if ((is.null(Z)) & (is.null(nRandComp) == FALSE)) {
    stop("Number of random components must be specified only when Z is defined")
  }
  if (is.null(Z) == FALSE) {
    if (dim(Z)[2] != sum(nRandComp)) {
      stop("The number of random effects in each random component must match with the number of columns of the design matrix Z")
    }
  }
  if ((method == "BB-NR") | (method == "rootSolve")) {
  }
  else {
    stop("The choosen estamation method is not adequate")
  }
  if (maxiter != as.integer(maxiter)) {
    stop("maxiter must be integer")
  }
  if (maxiter <= 0) {
    stop("maxiter must be positive")
  }
  if (sum(as.integer(m) == m) == length(m)) {
  }
  else {
    stop("m must be integer")
  }
  if (min(m) <= 0) {
    stop("m must be positive")
  }
  fixed.mf <- model.frame(formula = fixed.formula, data = data) # erstellt eigenen Datensatz mit den Variablen, die benötigt werden
  X <- model.matrix(attr(fixed.mf, "terms"), data = fixed.mf)
  q <- dim(X)[2]
  y <- model.response(fixed.mf) # nur y-Werte
  nObs <- length(y) # Anzahl Beobachtungen
  if (length(m) == 1) {
    balanced <- "yes"
    m. <- rep(m, nObs)
  }
  else {
    m. <- m
    if (sum(m[1] == m) == length(m)) {
      balanced <- "yes"
    }
    else {
      balanced <- "no"
    }
  }
  if (sum(as.integer(y) == y) == length(y)) {
  }
  else {
    stop("y must be integer")
  }
  if ((length(m) == 1) | (length(m) == length(y))) {
  }
  else {
    stop("m must be a number, or a vector of the length of y")
  }
  if (max(y - m) > 0 | min(y) < 0) {
    stop("y must be bounded between 0 and m")
  }
  if (is.null(random.formula)) {
    nComp <- length(nRandComp)
    nRand <- dim(Z)[2]
    namesRand <- as.character(seq(1, nComp, 1))
  }
  else {
    random.mf <- model.frame(formula = update(random.formula, 
                                              ~. - 1), data = data)
    nComp <- dim(random.mf)[2]
    nRandComp <- NULL
    Z <- NULL
    for (i in 1:nComp) {
      z <- model.matrix(~random.mf[, i] - 1)
      Z <- cbind(Z, z)
      nRandComp <- c(nRandComp, dim(z)[2])
    }
    nRand <- dim(Z)[2]
    namesRand <- names(random.mf)
  }
  iter <- 0
  ########################################################################################
  ### NEW ###
  BB <- BBreg(fixed.formula, m, data)
  beta <- start[[1]] # start parameters for beta
  u <- rep(0, nRand)
  phi <- BB$phi
  all.sigma <- rep(start[[2]], nComp) # start parameter for random effect variance
  ########################################################################################
  
  d <- d. <- NULL
  for (i in 1:nComp) {
    d <- c(d, rep(all.sigma[i], nRandComp[i]))
    d. <- c(d., rep(1/(all.sigma[i]), nRandComp[i]))
  }
  D <- diag(d)
  D. <- diag(d.)
  oldbetaphisigma <- rep(Inf, q + 1 + nComp)
  betaphisigma <- c(beta, phi, all.sigma)
  while (max(abs(betaphisigma - oldbetaphisigma)) > 0.001) {
    oldbetaphisigma <- betaphisigma
    if (method == "BB-NR") {
      rand.fix <- HRQoL:::EffectsEst.BBNR(y, m., beta, u, p, phi, 
                                  D., X, Z, maxiter)
      if (rand.fix$conv == "no") {
        print("The method has not converged")
        out <- list(conv = "no")
        return(out)
      }
    }
    else {
      rand.fix <- HRQoL:::EffectsEst.multiroot(y, m., beta, u, 
                                       p, phi, D., X, Z, maxiter)
      if (rand.fix$conv == "no") {
        print("The method has not converged")
        out <- list(conv = "no")
        return(out)
      }
    }
    beta <- rand.fix$fixed.est
    u <- rand.fix$random.est
    eta <- X %*% beta + Z %*% u
    p <- 1/(1 + exp(-eta))
    effects.iter <- rand.fix$iter
    thetaest <- HRQoL:::VarEst(y, m., p, X, Z, u, nRand, nComp, nRandComp, 
                       all.sigma, phi, q, maxiter)
    phi <- thetaest$phi
    all.sigma <- thetaest$all.sigma
    d. <- NULL
    for (i in 1:nComp) {
      d. <- c(d., rep(1/(all.sigma[i]), nRandComp[i]))
    }
    D. <- diag(d.)
    betaphisigma <- c(beta, phi, all.sigma)
    iter <- iter + 1
    cat("Iteration number:", iter, "\n")
  }
  fixed.vcov <- rand.fix$vcov.fixed
  all.sigma.var <- thetaest$all.sigma.var
  psi <- thetaest$psi
  psi.var <- thetaest$psi.var
  fitted.eta <- X %*% beta + Z %*% u
  fitted <- 1/(1 + exp(-(X %*% beta + Z %*% u)))
  conv <- "yes"
  e <- sum(y)/sum(m.)
  l1 <- l2 <- 0
  l1. <- l2. <- 0
  l1.null <- l2.null <- 0
  l3 <- 0
  for (j in 1:nObs) {
    t1 <- 0
    t1. <- 0
    t1.null <- 0
    if (y[j] == 0) {
    }
    else {
      for (k in 0:(y[j] - 1)) {
        t1 <- t1 + log(fitted[j] + k * phi)
        t1. <- t1. + log(y[j]/m.[j] + k * phi)
        t1.null <- t1.null + log(e + k * phi)
      }
    }
    l1 <- l1 + t1
    l1. <- l1. + t1.
    l1.null <- l1.null + t1.null
    t2 <- 0
    t2. <- 0
    t2.null <- 0
    if (y[j] == m.[j]) {
    }
    else {
      for (k in 0:(m.[j] - y[j] - 1)) {
        t2 <- t2 + log(1 - fitted[j] + k * phi)
        t2. <- t2. + log(1 - y[j]/m.[j] + k * phi)
        t2.null <- t2.null + log(1 - e + k * phi)
      }
    }
    l2 <- l2 + t2
    l2. <- l2. + t2.
    l2.null <- l2.null + t2.null
  }
  deviance <- -2 * ((l1 + l2) - (l1. + l2.))
  null.deviance <- -2 * ((l1.null + l2.null) - (l1. + l2.))
  df <- nObs - length(beta) - length(all.sigma) - 1
  null.df <- nObs - 1 - length(all.sigma) - 1
  out <- list(fixed.coef = beta, fixed.vcov = fixed.vcov, random.coef = u, 
              sigma.coef = all.sigma, sigma.var = all.sigma.var, phi.coef = phi, 
              psi.coef = psi, psi.var = psi.var, fitted.values = fitted, 
              conv = conv, deviance = deviance, df = df, null.deviance = null.deviance, 
              null.df = null.df, nRand = nRand, nRandComp = nRandComp, 
              namesRand = namesRand, iter = iter, nObs = nObs, y = y, 
              X = X, Z = Z, D = D, balanced = balanced, m = m, conv = conv)
  class(out) <- "BBmm"
  out$call <- match.call()
  out$formula <- formula
  out
}
