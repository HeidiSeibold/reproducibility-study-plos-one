# < Longitudinal influence of alcohol and marijuana use on academic performance in college students >

### Paper details

- Title: Longitudinal influence of alcohol and marijuana use on academic performance in college students
- Author(s): Shashwath A. Meda , Ralitza V. Gueorguieva, Brian Pittman, Rivkah R. Rosen, Farah Aslanzadeh, Howard Tennen, Samantha Leen, Keith Hawkins, Sarah Raskin, Rebecca M. Wood, Carol S. Austad, Alecia Dager, Carolyn Fallahi, Godfrey D. Pearlson
- Year: 2017
- Journal: PLOS|ONE
- DOI: e0172213
- Link: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0172213
- Data available (yes/no): yes
- Link to data: https://figshare.com/articles/BARCS_Academic_Substance_Use_Data/4617886/1
- Software used: SPSS v21 (http://www-01.ibm.com/software/analytics/spss/)
- Code available (yes/no): no
- Link to code: - 
- Contact e-mail: shashwath.meda@hhchealth.org

### Project details

- Student name(s): Maximilian Mandl, Melissa Schmoll, Jessica Peter