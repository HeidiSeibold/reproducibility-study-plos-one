all: paper_repro_study.pdf

paper_repro_study.pdf: paper_repro_study.tex references_repro_study.bib
	pdflatex paper_repro_study.tex
	bibtex paper_repro_study
	pdflatex paper_repro_study.tex
	pdflatex paper_repro_study.tex


paper_repro_study.tex: paper_repro_study.Rnw references_repro_study.bib
	Rscript \
		-e "library(knitr)" \
		-e "knitr::knit('paper_repro_study.Rnw')"

clean:
	rm paper_repro_study-concordance.tex
	rm paper_repro_study.bbl
	rm paper_repro_study.tex
	rm paper_repro_study.aux
	rm paper_repro_study.blg
	rm paper_repro_study.out

